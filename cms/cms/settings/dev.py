from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

from django.contrib.messages import constants as message_constants
MESSAGE_LEVEL = message_constants.DEBUG

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'd5++m$r#e0%jk+)izp1xo^v^bilza-9mi*7@&2mrwcnn@yn74!'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


try:
    from .local import *
except ImportError:
    pass
