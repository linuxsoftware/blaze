# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
import datetime as dt
from django.db import models
from django.utils import timezone

from wagtail.core.models import Page, Orderable
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel
from wagtail.images.edit_handlers import ImageChooserPanel
from modelcluster.fields import ParentalKey


class Speaker(Orderable):
    speaker = ParentalKey('symposium.ProgrammePage', related_name='speakers',
                          on_delete=models.CASCADE)
    name = models.CharField(max_length=80)
    image = models.ForeignKey('wagtailimages.Image',
                              null=True, blank=True,
                              on_delete=models.SET_NULL,
                              related_name='+')
    topic = models.CharField(max_length=80)
    on_date = models.DateField()
    start_dt = models.DateTimeField(help_text="Enter a datetime")
    finish_time = models.TimeField(null=True, blank=True)
    details  = RichTextField(blank=True)
    last_updated_dt = models.DateTimeField(default=timezone.now)

    panels = [FieldPanel('name'),
              ImageChooserPanel('image'),
              FieldPanel('topic'),
              FieldPanel('on_date'),
              FieldPanel('start_dt'),
              FieldPanel('finish_time'),
              FieldPanel('details', classname="full"),
             ]


class ProgrammePage(Page):
    content_panels = Page.content_panels + [
        InlinePanel('speakers', label="Speakers"),
        ]
