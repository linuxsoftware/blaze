# ------------------------------------------------------------------------------
# Events hooks
# ------------------------------------------------------------------------------

from django.contrib.staticfiles.templatetags.staticfiles import static
from django.utils.html import format_html
from django.utils import timezone
from wagtail.core import hooks

# ------------------------------------------------------------------------------
@hooks.register('before_serve_page')
def set_current_timezone(page, request, serve_args, serve_kwargs):
    print(timezone.get_current_timezone_name())
    return None

# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
